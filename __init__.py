from st3m.application import Application, ApplicationContext
import st3m.run
import leds
import os
import sys


class Geraffel(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.image_path = ""

        # FIXME: app_ctx.bundle_path does not work (yet) as of firmware v1.1.0
        # Workaround: Search known paths and app names
        paths = [path for path in sys.path if path.startswith("/")]
        app_names = ["011-geraffel"]

        for path in paths:
            for app_name in app_names:
                self.image_path = path + "/apps/" + app_name + "/geraffel-wings-resized.png"

                # Try to load image from known paths and use first result
                try:
                    os.stat(self.image_path)
                    break
                except OSError:
                    pass

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.image(self.image_path, -120, -120, 240, 240)

        for i in range(40):
            leds.set_rgb(i, 255,165,0)

        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing


if __name__ == '__main__':
    st3m.run.run_view(Geraffel(ApplicationContext()))
